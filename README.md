# Data Science Portfolio

Repository containing portfolio of data science projects completed by me for academic, self learning, and hobby purposes. Presented in the form of iPython Notebooks - Jupyter Notebook

# Contents

* ## Machine Learning
	* [Predicting Boston Housing Prices](https://gitlab.com/solomoons/data-science-porfolio/-/blob/master/Linear_and_Logistic_Regression%20with%20Boston%20House%20Price%20and%20MNIST%20DiGits%20data/Linear_and_Logistic_Regression.ipynb): A model to predict the value of a given house in the Boston real estate market using various statistical analysis tools. Identified the best price that a client can sell their house utilizing machine learning.
	* [Frau Detection Analysis](https://gitlab.com/solomoons/data-science-porfolio/-/blob/master/Frau%20Detection%20Analysis/fraud_detection.ipynb): An analysis and model to predict whether an activity is fraudulent or not. For each user, determine country based on the numeric IP address and assume this model which can be used live to predict in real time if an activity is fraudulent or not
	* [Employee Rentention](https://gitlab.com/solomoons/data-science-porfolio/-/blob/master/Employee%20Rentention/Employee%20Rentention%20Notebook.ipynb) :  Employee turn-over is a very costly problem for companies. The cost of replacing an employee if often larger than 100K USD, taking into account the time spent to interview and find a replacement, placement fees, sign-on bonuses and the loss of productivity for several months. This project try to nderstand why and when employees are most likely to leave can lead to actions to improve employee retention as well as planning new hiring in advance
	* [CNN - Reconize Lauging Man](https://gitlab.com/solomoons/data-science-porfolio/-/blob/master/CNN%20-%20Reconize%20Lauging%20Man/CNN%20exercise%20jupyter.ipynb): Using CNN to regconize the fake who is lauging or not , so we could guess he is happy or not
	* [Demo about dcGAN](https://gitlab.com/solomoons/data-science-porfolio/-/blob/master/Deep%20Convolutional%20Generative%20Adversarial%20Network%20-%20DCGAN%20demo/tensorflow-gan-demo.ipynb): implement the dcGAN to reveal the picture using tensorflow framework and dcGAN algotherms 


* ## Natural Language Processing
	* [Fake News](https://gitlab.com/solomoons/data-science-porfolio/-/blob/master/NLP%20-%20Fake%20news/fake%20news.ipynb) : Using **nltk** library and **sklearn** to evaluate the fake news infomation 


* ## Mini Project
	* [Software Sale Price](https://gitlab.com/solomoons/data-science-porfolio/-/blob/master/Software%20Sale%20Price/DSP_Mini_Projects_12_2019_02.ipynb): Goal Pricing optimization is, non surprisingly, another area where data science can provide huge value. The goal here is to evaluate whether a pricing test running on the site has been successful. As always, this focus on user segmentation and provide insights about segments who behave differently as well as any other insights.
	* [Potential Bus Stop](https://gitlab.com/solomoons/data-science-porfolio/-/blob/master/Potential%20Bus%20Stop/ModelEmpBus.ipynb): The company tried to open a bus using the public transportation of the city, our mission is to find out where to put the bus stop to optimize the number of employees could get .


* ## Marketing - RFM Modeling
	This content discuss about the RFM modeling : RFM stands for Recency - Frequency - Monetary Value
	* [Know your metric](https://gitlab.com/solomoons/data-science-porfolio/-/blob/master/Implement%20RFM%20Modeling/Know%20your%20metric/src/EDM%20online%20part1.ipynb)
	* [Customer Segmentation](https://gitlab.com/solomoons/data-science-porfolio/-/blob/master/Implement%20RFM%20Modeling/Customer%20Segmentation/g2_jpn.ipynb)
	* [Customer Lifetime Value Prediction](https://gitlab.com/solomoons/data-science-porfolio/-/blob/master/Implement%20RFM%20Modeling/Customer%20Lifetime%20Value%20Prediction/Customer%20Lifetime%20Value%20Prediction.ipynb)


* ## Kaggle Competition
	* [Blue Book for Bulldozers Kaggle Competition](https://gitlab.com/solomoons/data-science-porfolio/-/blob/master/Kaggle/Blue%20Book%20for%20Bulldozers/Blue-Book-for-Bulldozers.ipynb):The goal of the contest is to predict the sale price of a particular piece of heavy equiment at auction based on it's usage, equipment type, and configuration.  The data is sourced from auction result postings and includes information on usage and equipment configurations
	* **Kaggle House Prices: Advanced Regression Techniques**: This kernel is going to solve House Pricing with Advanced Regression Analysis, a popular machine learning dataset for beginners. This [analyse](https://gitlab.com/solomoons/data-science-porfolio/-/blob/master/Kaggle/House%20Prices%20-%20Advanced%20Regression%20Techniques/Eploring%20the%20file%20House%20Predics.ipynb) will visualze and have some understaing about data transform and the [model](https://gitlab.com/solomoons/data-science-porfolio/-/blob/master/Kaggle/House%20Prices%20-%20Advanced%20Regression%20Techniques/House%20Price%20Model.ipynb) will show us the prediction base on data transform above
	* [Pet Finder](https://gitlab.com/solomoons/data-science-porfolio/-/tree/master/Kaggle/Pet%20Finder): Millions of stray animals suffer on the streets or are euthanized in shelters every day around the world. If homes can be found for them, many precious lives can be saved — and more happy families created.
	PetFinder.my has been Malaysia’s leading animal welfare platform since 2008, with a database of more than 150,000 animals. PetFinder collaborates closely with animal lovers, media, corporations, and global organizations to improve animal welfare. Animal adoption rates are strongly correlated to the metadata associated with their online profiles, such as descriptive text and photo characteristics. As one example, PetFinder is currently experimenting with a simple AI tool called the Cuteness Meter, which ranks how cute a pet is based on qualities present in their photos.
	* [Mercari Price Suggestion Challenge](https://gitlab.com/solomoons/data-science-porfolio/-/tree/master/Kaggle/Mercari_Price_Suggestion_Project_Kaggle): Product pricing gets even harder at scale, considering just how many products are sold online. Clothing has strong seasonal pricing trends and is heavily influenced by brand names, while electronics have fluctuating prices based on product specs.Mercari, Japan’s biggest community-powered shopping app, knows this problem deeply. They’d like to offer pricing suggestions to sellers, but this is tough because their sellers are enabled to put just about anything, or any bundle of things, on Mercari's marketplace. Mercari’s challenging to build an algorithm that automatically suggests the right product prices.


If you liked what you saw, want to have a chat with me about the portfolio, work opportunities, or collaboration, shoot an email at thanhson91@gmail.com 