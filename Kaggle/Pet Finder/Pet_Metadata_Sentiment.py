#%%
import pandas as pd
import numpy as np
import glob
import json
from joblib import Parallel, delayed
from tqdm import tqdm, tqdm_notebook
#%%
train_image_files = sorted(glob.glob('Mini Project 11/petfinder-adoption-prediction/train_images/*.jpg'))
train_metadata_files = sorted(glob.glob('Mini Project 11/petfinder-adoption-prediction/train_metadata/*.json'))
train_sentiment_files = sorted(glob.glob('Mini Project 11/petfinder-adoption-prediction/train_sentiment/*.json'))

print(f'Numbers of train images files: {len(train_image_files)}')
print(f'Numbers of train metadata files: {len(train_metadata_files)}')
print(f'Numbers of train sentiment files: {len(train_sentiment_files)}')


test_image_files = sorted(glob.glob('Mini Project 11/petfinder-adoption-prediction/test_images/*.jpg'))
test_metadata_files = sorted(glob.glob('Mini Project 11/petfinder-adoption-prediction/test_metadata/*.json'))
test_sentiment_files = sorted(glob.glob('Mini Project 11/petfinder-adoption-prediction/test_sentiment/*.json'))

print(f'Numbers of test images files: {len(test_image_files)}')
print(f'Numbers of test metadata files: {len(test_metadata_files)}')
print(f'Numbers of test sentiment files: {len(test_sentiment_files)}')

# %%
# Read all Data
breeds = pd.read_csv('Mini Project 11/petfinder-adoption-prediction/breed_labels.csv')
colors = pd.read_csv('Mini Project 11/petfinder-adoption-prediction/color_labels.csv')
states = pd.read_csv('Mini Project 11/petfinder-adoption-prediction/state_labels.csv')

train = pd.read_csv('Mini Project 11/petfinder-adoption-prediction/train.csv')
test = pd.read_csv('Mini Project 11/petfinder-adoption-prediction/test/test.csv')
sub = pd.read_csv('Mini Project 11/petfinder-adoption-prediction/test/sample_submission.csv')
#%%
train_id = train[['PetID']]
train_id
#%% [markdown]
# Tap Train
#%%
# Metadata
train_metadata = pd.DataFrame(train_metadata_files)
train_metadata.columns = ['metadata_filename']
train_metadata_pets = train_metadata['metadata_filename'].apply(lambda x: x.split('/')[-1].split('-')[0])
train_metadata = train_metadata.assign(PetID=train_metadata_pets)
print(len(train_metadata_pets.unique()))

pets_with_metadatas = len(np.intersect1d(train_metadata_pets.unique(), train_id['PetID'].unique()))
print(f'Fraction of pets with metadata: {pets_with_metadatas / train_id.shape[0]:.3f}')
#%%
# Sentiment:
train_sentiment = pd.DataFrame(train_sentiment_files)
train_sentiment.columns = ['sentiment_filename']
train_sentiment_pets = train_sentiment['sentiment_filename'].apply(lambda x: x.split('/')[-1].split('.')[0])
train_sentiment = train_sentiment.assign(PetID=train_sentiment_pets)
print(len(train_sentiment_pets.unique()))

pets_with_sentiments = len(np.intersect1d(train_sentiment_pets.unique(), train_id['PetID'].unique()))
print(f'fraction of pets with sentiment: {pets_with_sentiments / train_id.shape[0]:.3f}')

#%% [markdown]
# Tap Test
#%%
test_id = test[['PetID']]
print(test_id.shape)
# Metadata:
test_metadata = pd.DataFrame(test_metadata_files)
test_metadata.columns = ['metadata_filename']
test_metadata_pets = test_metadata['metadata_filename'].apply(lambda x: x.split('/')[-1].split('-')[0])
test_metadata = test_metadata.assign(PetID=test_metadata_pets)
print(len(test_metadata_pets.unique()))

pets_with_metadatas = len(np.intersect1d(test_metadata_pets.unique(), test_id['PetID'].unique()))
print(f'Fraction of pets with metadata: {pets_with_metadatas / test_id.shape[0]:.3f}')

# Sentiment:
test_sentiment = pd.DataFrame(test_sentiment_files)
test_sentiment.columns = ['sentiment_filename']
test_sentiment_pets = test_sentiment['sentiment_filename'].apply(lambda x: x.split('/')[-1].split('.')[0])
test_sentiment = test_sentiment.assign(PetID=test_sentiment_pets)
print(len(test_sentiment_pets.unique()))

pets_with_sentiments = len(np.intersect1d(test_sentiment_pets.unique(), test_id['PetID'].unique()))
print(f'fraction of pets with sentiment: {pets_with_sentiments / test_id.shape[0]:.3f}')

#%% [markdown]
# Extract Features from Json file
#%%
class PetFinderParser(object):

    def __init__(self, debug=False):

        self.debug = debug
        self.sentence_sep = ' '

        self.extract_sentiment_text = False

    def open_json_file(self, filename):
        with open(filename, 'r', encoding='utf-8') as f:
            json_file = json.load(f)
        return json_file

    def parse_sentiment_file(self, file):
        """
        Parse sentiment file. Output DF with sentiment features.
        """

        file_sentiment = file['documentSentiment']
        file_entities = [x['name'] for x in file['entities']]
        file_entities = self.sentence_sep.join(file_entities)

        file_sentences_sentiment = [x['sentiment'] for x in file['sentences']]

        file_sentences_sentiment = pd.DataFrame.from_dict(
            file_sentences_sentiment, orient='columns')
        file_sentences_sentiment_df = pd.DataFrame(
            {
                'magnitude_sum': file_sentences_sentiment['magnitude'].sum(axis=0),
                'score_sum': file_sentences_sentiment['score'].sum(axis=0),
                'magnitude_mean': file_sentences_sentiment['magnitude'].mean(axis=0),
                'score_mean': file_sentences_sentiment['score'].mean(axis=0),
                'magnitude_var': file_sentences_sentiment['magnitude'].var(axis=0),
                'score_var': file_sentences_sentiment['score'].var(axis=0),
                'magnitude_std': file_sentences_sentiment['magnitude'].std(axis=0),
                'score_std': file_sentences_sentiment['score'].std(axis=0),
            }, index=[0]
        )

        df_sentiment = pd.DataFrame.from_dict(file_sentiment, orient='index').T
        df_sentiment = pd.concat([df_sentiment, file_sentences_sentiment_df], axis=1)

        df_sentiment['entities'] = file_entities
        df_sentiment = df_sentiment.add_prefix('sentiment_')

        return df_sentiment

    def parse_metadata_file(self, file):
        """
        Parse metadata file. Output DF with metadata features.
        """

        file_keys = list(file.keys())

        if 'labelAnnotations' in file_keys:
            file_annots = file['labelAnnotations']
            file_top_score = np.asarray([x['score'] for x in file_annots]).mean()
            file_top_desc = [x['description'] for x in file_annots]
        else:
            file_top_score = np.nan
            file_top_desc = ['']

        file_colors = file['imagePropertiesAnnotation']['dominantColors']['colors']
        file_crops = file['cropHintsAnnotation']['cropHints']

        file_color_score = np.asarray([x['score'] for x in file_colors]).mean()
        file_color_pixelfrac = np.asarray([x['pixelFraction'] for x in file_colors]).mean()

        file_crop_conf = np.asarray([x['confidence'] for x in file_crops]).mean()

        if 'importanceFraction' in file_crops[0].keys():
            file_crop_importance = np.asarray([x['importanceFraction'] for x in file_crops]).mean()
        else:
            file_crop_importance = np.nan

        df_metadata = {
            'annots_score': file_top_score,
            'color_score': file_color_score,
            'color_pixelfrac': file_color_pixelfrac,
            'crop_conf': file_crop_conf,
            'crop_importance': file_crop_importance,
            'annots_top_desc': self.sentence_sep.join(file_top_desc)
        }

        df_metadata = pd.DataFrame.from_dict(df_metadata, orient='index').T
        df_metadata = df_metadata.add_prefix('metadata_')

        return df_metadata


def extract_additional_features(pet_id, mode='train'):

    sentiment_filename = f'Mini Project 11/petfinder-adoption-prediction/{mode}_sentiment/{pet_id}.json'
    try:
        sentiment_file = pet_parser.open_json_file(sentiment_filename)
        df_sentiment = pet_parser.parse_sentiment_file(sentiment_file)
        df_sentiment['PetID'] = pet_id
    except FileNotFoundError:
        df_sentiment = []

    dfs_metadata = []
    metadata_filenames = sorted(glob.glob(f'Mini Project 11/petfinder-adoption-prediction/{mode}_metadata/{pet_id}*.json'))
    if len(metadata_filenames) > 0:
        for f in metadata_filenames:
            metadata_file = pet_parser.open_json_file(f)
            df_metadata = pet_parser.parse_metadata_file(metadata_file)
            df_metadata['PetID'] = pet_id
            dfs_metadata.append(df_metadata)
        dfs_metadata = pd.concat(dfs_metadata, ignore_index=True, sort=False)
    dfs = [df_sentiment, dfs_metadata]

    return dfs
#%%
pet_parser = PetFinderParser()

#%%
# Hide code de ko bi chay lai model khi run cells
# debug = False
# train_pet_ids = train.PetID.unique()
# test_pet_ids = test.PetID.unique()

# if debug:
#     train_pet_ids = train_pet_ids[:1000]
#     test_pet_ids = test_pet_ids[:500]


# dfs_train = Parallel(n_jobs=-1, verbose=1)(
#     delayed(extract_additional_features)(i, mode='train') for i in train_pet_ids)

# train_dfs_sentiment = [x[0] for x in dfs_train if isinstance(x[0], pd.DataFrame)]
# train_dfs_metadata = [x[1] for x in dfs_train if isinstance(x[1], pd.DataFrame)]

# train_dfs_sentiment = pd.concat(train_dfs_sentiment, ignore_index=True, sort=False)
# train_dfs_metadata = pd.concat(train_dfs_metadata, ignore_index=True, sort=False)

# print(train_dfs_sentiment.shape, train_dfs_metadata.shape)


# dfs_test = Parallel(n_jobs=-1, verbose=1)(
#     delayed(extract_additional_features)(i, mode='test') for i in test_pet_ids)

# test_dfs_sentiment = [x[0] for x in dfs_test if isinstance(x[0], pd.DataFrame)]
# test_dfs_metadata = [x[1] for x in dfs_test if isinstance(x[1], pd.DataFrame)]

# test_dfs_sentiment = pd.concat(test_dfs_sentiment, ignore_index=True, sort=False)
# test_dfs_metadata = pd.concat(test_dfs_metadata, ignore_index=True, sort=False)

# print(test_dfs_sentiment.shape, test_dfs_metadata.shape)

# %% [markdown]
Code o tren chay model Parallel de giam thoi gian xu ly so voi vong lap thong thuong
Note: Chay rat lau
#%%
train_dfs_sentiment.head(5)

# %%
# Luu lai du lieu da chay ra ket qua truoc khi lam tiep
# path_folder = 'Mini Project 11/Working Folder/DataExtraction/'
# train_dfs_sentiment.to_csv(path_folder + 'train_dsf_sentiment.csv', index=False)
# train_dfs_metadata.to_csv(path_folder + 'train_dfs_metadata.csv',index=False)
# test_dfs_sentiment.to_csv(path_folder + 'test_dfs_sentiment.csv',index=False)
# test_dfs_metadata.to_csv(path_folder + 'test_dfs_metadata.csv',index=False)
